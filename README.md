# Deep Learning repo for Machine Learning Course UTFSM

## Deep Learning libro guía

Utilize como guia teórica la biblia del deep learning. 
> Deep Learning, Goodfellow. Disponible en: https://www.deeplearningbook.org/

### grad_dest

Estudio de como en gradiente descendente funciona en python. Usado como testing para el capitulo V del libro guia deep.

## deeplearning_chollet

Implementación del Chollet, códigos en keras. Códigos generales de implementación de varios tiṕos de redes y configuraciones deep. Usado para los capitulos VI hacia delante.
> Deep Learning with Python, Francois Chollet.

## datasc_handbook

Algoritmos básicos de data science en python relacionados con manejo en pandas, matplotlib y numpy. Generalidades de implemetación de todos los tipos. Basado en Libro:
> Python Data Science Handbook, Jake VanderPlas.

usado como complemento a todos los capitulos del libro para entender Python y como trabajar en este contexto con él.
